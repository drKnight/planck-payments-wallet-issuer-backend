{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE NoImplicitPrelude, OverloadedStrings, DeriveGeneric #-}

module TestingKeys (loadPrivateKey1, loadPublicKey1) where

import Prelude
import Data.X509.Memory (readKeyFileFromMemory)
import Crypto.KeyManagement (PrivateKey, PublicKey, readPublicKeyPEM)
import Data.X509 (PrivKey(PrivKeyRSA))
import Data.ByteString (ByteString)


privateKey1BS :: ByteString
privateKey1BS =
  "-----BEGIN RSA PRIVATE KEY-----\n\
  \MIIJKAIBAAKCAgEA1ktgpoYJdnz/XYuVmLQuKXra25mcfu6RTRSzmpZNoOLz9nXo\n\
  \ZBlUm3pGzPcgUvpUDWKh5hwQxG7tOutIDTn9/ROvipTGDBmjtWf/X7TgqtLdJz98\n\
  \fNabmBxxkssFUt5F55d7dV/bcG8bsQJTh+WPVhdbBsS8hMSTionyf98aoAdW3S1I\n\
  \NH1b9KVWBY6gG+/7pM9v/pDh+gnXAWbqzsmfniirmQH2XQE1k1aqRceo8XW9rreU\n\
  \pQ4fPyitT1j9E5+NGDrqBn6Jfqcaq0JLoCFTFgskbc2Ye2ucbbI5cifaFxROyV71\n\
  \v4QLRafvweRWEKfoICBTsBOymftBOoQw/DtcGvvOijLszraiViqHvBY2LtZ+NFAc\n\
  \3Ys5mxw+ZZDXGCdoVHFbug1BcZ2LKFUnfQil0kKXa5D0OA7GP/7//94rjuANSTaI\n\
  \WwFP2+CouFQwu2hTjJnxCj5NAVvV85pVTV9bcSdFRv44gDXTjADqlqXWzAQ8wPeC\n\
  \Efux0+9YUo6Uo87Im+1SJ9TX8LFZjhTi91BbTC3s0YFLbCPUDYkmzohBIJYmxsy+\n\
  \g6nYOrwum3qbWYsn4Nt+7SszqncpENioJv2Ft7KvR2QeCxLMja0sB3Lku7wPsU89\n\
  \tnn+QOTq+S64bB1GlRJQtDPiGcD/ngR0diihPionlj9TH1SQNcBoEEYXYb0CAwEA\n\
  \AQKCAgA3EgPyh3ujEZhmp9p39cXvLv1TdfR7wj9g+bL2RS4J7IkZi09/eer+Ubwk\n\
  \LFYIPQCgt/o3Mns0n/UjMRfBHmghQ4/XI0iKD4S8t88u/0LFY48l6PDK/DPcMWMW\n\
  \gEoL88cvLwSxM0Mnb8ZEBP8Ga34cd8ASyzhPPl1UOwMwzVsASSKPkHV/179r7Zlf\n\
  \sDmBNHkM76ouV3lbUnAIkjRBlBsLABSpK5HucMMd7CWx7Q+DjeiDH/Z5PFnXV1LC\n\
  \k3HmOxh9Y5aEuLwI6lS2MkFRE39QqOhht3Y6MXtvzV9p8MfhKnzczySEZSCoc/Hv\n\
  \eAiOGzhSGTyogUdivukSaqpUB8UxSTqNrqi/l61AjTrcvTo+VMEBrId4ZO2ozEmL\n\
  \Rqw9FdRwruRMW74egjiCS+zcWVzgEJf3m7cWYIdJvHMKYTvg2ic3iiJvwF9d51Jk\n\
  \V2nFMbsrZkZQTTL33JT99eTGDu/bju5Wep/P4eo3T7xSstJl+FY5de79WBpgXTwz\n\
  \XtX8k+ABef0799gb55nJhS4uvlWhZLFG3Zw3ImKLumku0Jf0NTw9N64If+xLqic8\n\
  \y1UNCEmzwdI9cRhZKVbXPoDgYHLw38IiEslCXNdmXCmYxt5A8vdnJhFnGOQFeCqi\n\
  \On+7XgfqEsNab2ZVgS+48juA1pIkgFjQnAXmlzNAdH+xNdD8mQKCAQEA/qnil93n\n\
  \HGUFeF5eL/kH1kSH/vxw7Ol93Tsmr6XEHivsDeuOf0Cms4sJjyVvIML90xQ27DrY\n\
  \MLKlVC8NW8YnnsCL4QAxD7//AP+DaZIz4JDw7lwFevMfI5gvU2OsATwP6af2fHEv\n\
  \ADBkX/Ux+Lm1egV3Q3MtxPpGln/rkd7iHFsV8dbHKpDFP4/rUpmbHIUZIRtG/Q3k\n\
  \Fl7vRcjPttUZb7nocZiBVYbuIrULuyAANWMqrzDAxzNt+tAjGajEvNtTCW2aSH0o\n\
  \e4tNSXI7lgvvzbwNKzpJSjxcMOWDV4YU4pdrKxqLzLtydoX+WQEotH862x0BojL8\n\
  \Q5dDVAFv74vqFwKCAQEA12tCsGSB/6RG9ebxXoxCpl9VGxaPgwNER+JjwHWFAn6N\n\
  \ABsXZhSUT2ZoszvHtVei1J+BUUug3nJL9xQWtjl5SCtN1giqzDaqGErCKKJKMLgK\n\
  \ws/s6AZbtKgB0vS7XT4EBY7iI78SMa8Nk5aJvz7xGyk/AAV6dxvdnAVqb+AsfrVO\n\
  \3ewSXka/d6jY6thNkneByBqZdcDhHbIwwyfdSkBFFm2PlyOyKiu7GO3btZqn1p+n\n\
  \MWmizzm/4U5uiAB2/uucpc5XJxGuyZIgz3b2Yq7obYQJWdf/UNigktKgDJxBzPsa\n\
  \DetyQ5IyEzgjwl2FDfFjAEjqscJvK/CXl2o3zJ27SwKCAQA9wsm3dxOTqVRYStqg\n\
  \285wwpIQ3vwsGLxzqOLK+nLDtnc7MQOQDQfgxzl8Q+HdmiygwmhHhkXkGY9LCVY+\n\
  \CaKWN7A00y0S2sdnYJBoTBLUxZII3LxfgxLsOP5RRPz0F7LsQdXffDuf2hydwQNr\n\
  \hO5kN2sjKo3RFbmEymVBLUK9CcIasrH+url7ntW9ZI9frU/Y3ZqryHCmlEOHMxpd\n\
  \BvVySS38TrYfHegm9q1vpmUmjXROpI48QVbM2ymIEYwVGUPcJ36hUek9fR0zUMbu\n\
  \rtprlM3EzA/2NwlMunzN/zCVe498FqYM8vDhmcGDqGpMzAZkGXbJ9X+EjOzBNNAG\n\
  \0nHlAoIBAQCLn9wT1LFl7yaHT+72Z1bz2yqgahkOw9lH9nbNUd6qZTgi72xVH9WZ\n\
  \OTl3orGo2mqSAeihtrFIPTD7rquh5NK1ckv7BJcf0fah1CLQj5Aiz8c5yec15Jkg\n\
  \GR1FjJa8suuMZrYwkTHLvjjAhnw11ygTgMJy0xozhX44GFKKvPRaFE5QDSlCrfyA\n\
  \JJKkdRqiao9WEdeRuTiwNMTiWT5XWBzvkofVYK+kIcAdQl578QgEVmrZ3ixGwJmc\n\
  \AWgnApYeWLypw3o9unpd8DcQAX1lLRU9lq0w7I1x61b4k0hlpAoKQF780rS3E1Ud\n\
  \m++hlVgeV2zhzGFCYlIIWuMGozQTjdDNAoIBAERU6HXLfI7ZL6340S8DtHQfdCIS\n\
  \8VMNxYdDk4hRDb+HSDopyIxUxvp7WjACoSF0YH2ceUB97ICQNAB4FsAJ1PYqM8qY\n\
  \nuLh0Aeh8FFd7KYAccglkXMm1PtVPbvTld3Zse/bdyn40s3kkoWsEkEc9bF0hy3B\n\
  \WwlsVrCl+FJ7Bl/Q5dyg0ON2XvJ90Tuutk2ImQ/Hawscy3+y6ALd/bkh8TOxE63r\n\
  \gUj0NvYrPvFgDU1I/zN91lgQEOgE3HoOEa+ltcOj6mBcTCtlMCFUlwO2c1nL48YM\n\
  \Z5u9B+KbAhtApncgRtsoyMXy+VCYSZNBb7nz221+hhBBPYpITP9eYgcJkr8=\n\
  \-----END RSA PRIVATE KEY-----"

publicKey1BS :: ByteString
publicKey1BS =
  "-----BEGIN PUBLIC KEY-----\n\
  \MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA1ktgpoYJdnz/XYuVmLQu\n\
  \KXra25mcfu6RTRSzmpZNoOLz9nXoZBlUm3pGzPcgUvpUDWKh5hwQxG7tOutIDTn9\n\
  \/ROvipTGDBmjtWf/X7TgqtLdJz98fNabmBxxkssFUt5F55d7dV/bcG8bsQJTh+WP\n\
  \VhdbBsS8hMSTionyf98aoAdW3S1INH1b9KVWBY6gG+/7pM9v/pDh+gnXAWbqzsmf\n\
  \niirmQH2XQE1k1aqRceo8XW9rreUpQ4fPyitT1j9E5+NGDrqBn6Jfqcaq0JLoCFT\n\
  \Fgskbc2Ye2ucbbI5cifaFxROyV71v4QLRafvweRWEKfoICBTsBOymftBOoQw/Dtc\n\
  \GvvOijLszraiViqHvBY2LtZ+NFAc3Ys5mxw+ZZDXGCdoVHFbug1BcZ2LKFUnfQil\n\
  \0kKXa5D0OA7GP/7//94rjuANSTaIWwFP2+CouFQwu2hTjJnxCj5NAVvV85pVTV9b\n\
  \cSdFRv44gDXTjADqlqXWzAQ8wPeCEfux0+9YUo6Uo87Im+1SJ9TX8LFZjhTi91Bb\n\
  \TC3s0YFLbCPUDYkmzohBIJYmxsy+g6nYOrwum3qbWYsn4Nt+7SszqncpENioJv2F\n\
  \t7KvR2QeCxLMja0sB3Lku7wPsU89tnn+QOTq+S64bB1GlRJQtDPiGcD/ngR0diih\n\
  \Pionlj9TH1SQNcBoEEYXYb0CAwEAAQ==\n\
  \-----END PUBLIC KEY-----"


loadPrivateKey1 :: Either String PrivateKey
loadPrivateKey1 = do
  case readKeyFileFromMemory privateKey1BS of
    [PrivKeyRSA privateKey] -> return privateKey
    _ -> Left "malformed private key"


loadPublicKey1 :: Either String PublicKey
loadPublicKey1 = readPublicKeyPEM publicKey1BS
