{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE NoImplicitPrelude, OverloadedStrings #-}

module DB.PublicKeys (storePublicKey, retrievePublicKey) where

import Prelude

import DB.Internals.Base (withConnection)
import Hasql.Statement (Statement(Statement))
import qualified Data.ByteString.Char8 as BS_C8
import System.Environment ( getEnv )
import qualified DB.Internals.Params as Params
import qualified DB.Internals.Result as Result
import qualified Hasql.Decoders as Decoders
import qualified Hasql.Session as Session
import qualified Hasql.Transaction as Transaction
import Hasql.Transaction.Sessions (transaction, Mode(Read,Write), IsolationLevel(ReadCommitted))
import Data.UUID (UUID)
import Crypto.KeyManagement (PublicKey)

import Contravariant.Extras.Contrazip (contrazip2)

withPublickeyConnection :: Session.Session a -> IO a
withPublickeyConnection session = do
  publicKeyDB <- BS_C8.pack <$> getEnv "PUBLICKEY_DB"
  withConnection publicKeyDB session


storePublicKeyStatement :: Statement (UUID, PublicKey) ()
storePublicKeyStatement =
  Statement
    "INSERT INTO keys VALUES ($1, $2)"
    (contrazip2 Params.uuid Params.publicKey)
    Decoders.noResult
    True

storePublicKey :: UUID -> PublicKey -> IO ()
storePublicKey walletId publicKey =
  let
    t = Transaction.statement (walletId, publicKey) storePublicKeyStatement
    session = transaction ReadCommitted Write t
  in
    withPublickeyConnection session


retrievePublicKeyStatement :: Statement UUID PublicKey
retrievePublicKeyStatement =
  Statement
    "SELECT public_key FROM keys WHERE wallet_id = $1"
    Params.uuid
    Result.publicKey
    True

retrievePublicKey :: UUID -> IO PublicKey
retrievePublicKey walletId =
  let
    t = Transaction.statement walletId retrievePublicKeyStatement
    session = transaction ReadCommitted Read t
  in
    withPublickeyConnection session
