{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE OverloadedStrings, NoImplicitPrelude, TemplateHaskell #-}

module Data.PlanckPayments.BalanceRequest (BalanceRequest(BalanceRequest), walletId) where

import Data.UUID (UUID)
import Data.Aeson ((.=))
import Data.Aeson.TH (deriveJSON, defaultOptions)
import Data.JWT (FromJWT, ToJWT, toClaims)

data BalanceRequest = BalanceRequest {
  walletId :: !UUID
}

$(deriveJSON defaultOptions 'BalanceRequest)

instance FromJWT BalanceRequest

instance ToJWT BalanceRequest where
  toClaims req =
    ( "walletId" .= walletId req )
