{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE OverloadedStrings, NoImplicitPrelude, TemplateHaskell #-}

module Data.PlanckPayments.WalletRefillRequest (WalletRefillRequest(WalletRefillRequest), walletId, value, prepaidCode) where

import Prelude

import qualified Data.UUID
import qualified Data.Int

import Data.Aeson ((.=))
import Data.Aeson.TH (deriveJSON, defaultOptions)

import Data.JWT (FromJWT, ToJWT, toClaims)

type UUID = Data.UUID.UUID
type Int32 = Data.Int.Int32

data WalletRefillRequest = WalletRefillRequest
  { walletId :: !UUID
  , value :: !Int32
  , prepaidCode :: !UUID
  }

$(deriveJSON defaultOptions 'WalletRefillRequest)

instance FromJWT WalletRefillRequest

instance ToJWT WalletRefillRequest where
  toClaims req =
    ( "walletId"    .= walletId req
    <>"value"       .= value req
    <>"prepaidCode" .= prepaidCode req
    )
