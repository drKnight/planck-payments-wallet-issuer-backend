{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE NoImplicitPrelude, OverloadedStrings #-}

module Data.JWT (ToJWT, FromJWT, toClaims, sign, sign', verify, verify', peekClaim) where

import Prelude

import Data.ByteString (ByteString)
import Data.ByteString.Internal (c2w)
import Data.ByteString.Base64.URL (decodeBase64, encodeBase64Unpadded')
import Data.Aeson (Value(Object), Series, ToJSON, FromJSON, parseJSON)
import Data.Aeson.Types (parseEither)
import Data.Bifunctor (first)
import Data.Text (Text)
import qualified Data.Binary.Builder as Builder
import qualified Data.HashMap.Strict as HashMap
import qualified Data.Aeson as Aeson
import qualified Data.Text as Text
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS


import Crypto.Hash.Algorithms (SHA512(SHA512))
import qualified Crypto.PubKey.RSA.PKCS15

import Crypto.KeyManagement (getPrivateKey, getPublicKey, PublicKey, PrivateKey)

class ToJSON a => ToJWT a where
  toClaims :: a -> Series


class FromJSON a => FromJWT a


claimsByteString :: ToJWT a => a -> ByteString
claimsByteString =
   LBS.toStrict . Builder.toLazyByteString . Aeson.fromEncoding . Aeson.pairs . toClaims


sign :: ToJWT a => a -> IO ByteString
sign claims = do
  privateKey <- getPrivateKey
  sign' privateKey claims

sign' :: ToJWT a => PrivateKey -> a -> IO ByteString
sign' privateKey value =
  let
    header :: ByteString
    header = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9" -- {"alg": "RS512","typ": "JWT"}

    claims :: ByteString
    claims = encodeBase64Unpadded' $ claimsByteString value

    body :: ByteString
    body = header <> "." <> claims

  in do
    signatureResult <- Crypto.PubKey.RSA.PKCS15.signSafer (Just SHA512) privateKey body
    signature <- either (fail . show) return signatureResult
    return $ body <> "." <> encodeBase64Unpadded' signature


splitJWT :: ByteString -> Either String (ByteString, ByteString, ByteString)
splitJWT jwtByteString =
  case BS.split (c2w '.') jwtByteString of
    [header64, claims64, signature64] -> Right (header64, claims64, signature64)
    _ -> Left "Malformed JWT, must consist of 3 sections separated by dots"


readClaims :: FromJWT a => ByteString -> Either String a
readClaims claimsB64 = do
  claimsBS <- first Text.unpack $ decodeBase64 claimsB64
  Aeson.eitherDecodeStrict claimsBS

verify :: FromJWT a => ByteString -> IO a
verify jwtByteString = do
  publicKey <- getPublicKey
  verify' publicKey jwtByteString

verify' :: FromJWT a => PublicKey -> ByteString -> IO a
verify' publicKey jwtByteString =
  do
    (header64, claims64, signature64) <- either fail return $ splitJWT jwtByteString
    signature                         <- either (fail . Text.unpack) return $ Data.ByteString.Base64.URL.decodeBase64 signature64

    if Crypto.PubKey.RSA.PKCS15.verify (Just SHA512) publicKey (header64 <> "." <> claims64) signature
      then
        case readClaims claims64 of
          Right claims -> return claims
          Left errMsg -> fail errMsg
      else fail $ "verification failed."

peekClaim :: FromJSON a => Text -> ByteString -> IO a
peekClaim claimName jwtBS =
  do
    (_, claimsB64, _) <- either fail return $ splitJWT jwtBS
    claimsBS <- either (fail . Text.unpack) return $ decodeBase64 claimsB64
    value <- either fail return $ Aeson.eitherDecodeStrict claimsBS
    claimValue <- case value of
      Object o -> case HashMap.lookup claimName o of
        Just v -> return v
        Nothing -> fail $ "Could not find claim" <> Text.unpack claimName
      _ -> fail "Claims are not key-value pairs"
    claim <- either fail return $ parseEither parseJSON claimValue
    return claim
